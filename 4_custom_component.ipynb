{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to: Build a custom component\n",
    "\n",
    "This notebook aids you through creating a simple custom component which you can add to a spaCy pipeline. Your component will be able to detect the perspective of a text, i.e. whether the text has a 1st-, 2nd-, or 3rd-person narrator.\n",
    "\n",
    "For example, the following text (which is the beginning of August Bürger's *Münchhausen*) has a 1st-person narrator, which can be clearly seen by the frequent use of the 1st-person pronoun *Ich*:\n",
    "\n",
    "*<u>Ich</u> trat meine Reise nach Rußland von Haus ab mitten im Winter an, weil <u>ich</u> ganz richtig schloß, daß Frost und Schnee die Wege durch die nördlichen Gegenden von Deutschland, Polen, Kur- und Livland, welche nach der Beschreibung aller Reisenden fast noch elender sind als die Wege nach dem Tempel der Tugend, endlich, ohne besondere Kosten hochpreislicher, wohlfürsorgender Landesregierungen, ausbessern müßte. <u>Ich</u> reisete zu Pferde, welches, wenn es sonst nur gut um Gaul und Reiter steht, die bequemste Art zu reisen ist. Denn man riskiert alsdann weder mit irgendeinem höflichen deutschen Postmeister eine Affaire d'honneur zu bekommen, noch von seinem durstigen Postillion vor jede Schenke geschleppt zu werden.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we start, we need some imports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9f2d9ded",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2023-04-14 19:41:02.440534: W tensorflow/stream_executor/platform/default/dso_loader.cc:59] Could not load dynamic library 'libcudart.so.10.1'; dlerror: libcudart.so.10.1: cannot open shared object file: No such file or directory\n",
      "2023-04-14 19:41:02.440555: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.\n"
     ]
    }
   ],
   "source": [
    "# general imports\n",
    "import os\n",
    "import sys\n",
    "\n",
    "# import spacy functionalities\n",
    "import spacy\n",
    "from spacy.tokens import Doc, Span, Token\n",
    "\n",
    "# import `add_extension` (explained below)\n",
    "sys.path.append(os.path.join(os.getcwd(), \"../pipy-public\"))\n",
    "from pipeline.utils_methods import add_extension\n",
    "\n",
    "# import TextGrid components\n",
    "from tgclients.aggregator import Aggregator\n",
    "aggregator = Aggregator()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7459bc51",
   "metadata": {},
   "source": [
    "A pipeline component is a method that adds or modifies information in a `Doc` object. Specifically, it takes a `Doc` object as input and returns the same `Doc` object. New information added by the pipeline component is usually stored in new attributes, either attributes of the document or of spans or tokens within the document.\n",
    "\n",
    "The method `add_extension`, which we import above, can be used to initialise a new custom attribute. The usuage is illustrated below, where we call `add_extension(Doc, \"perspective\")` to define the custom attribute `\"perspective\"` for `Doc` objects (it works likewise for `Span` and `Token` objects). From now on, `doc._.perspective` is accessible (the value is `None` per default). `add_extension` internally calls spaCy's `set_extension` method but is a bit more convenient to use (e.g. because `set_extension` can only be called outside of pipeline components)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "cff23dfb",
   "metadata": {},
   "outputs": [],
   "source": [
    "def rb_perspective_tagger(doc):\n",
    "    \"\"\"Spacy pipeline component.\n",
    "        Adds the perspective of the text's narrator (\"1st\", \"2nd\" or \"3rd\") to the document.\n",
    "\n",
    "    Args:\n",
    "        doc (`Doc`): A spacy document object.\n",
    "    \n",
    "    Returns:\n",
    "        `Doc`: A spacy document object.\n",
    "\n",
    "    \"\"\"\n",
    "    add_extension(Doc, \"perspective\")\n",
    "    # #################################\n",
    "    # Change this code and add your own\n",
    "    doc._.perspective = \"3rd\"\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "    # #################################\n",
    "    return doc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d8a35e7",
   "metadata": {},
   "source": [
    "As you can see, the method `rb_perspective_tagger` (`rb_` stands for *rule-based*) assigns the value `\"3rd\"` to `doc._.perspective`. Although many texts have a 3rd-person narrator, this is probably not a good perspective tagger. You now have time to improve it by changing the code above.\n",
    "\n",
    "These ideas might help you:\n",
    "- Perspective is often encoded by pronouns (like *ich* 'I', *mich* 'me', *er* 'he', or *ihm* 'him').\n",
    "- The narrator, and therefore also perspective, might change in parts of the text. We want to tag only the perspective of the \"main\" narrator (e.g. the most prominent narrator).\n",
    "- You can use information provided by other pipeline components, e.g. part-of-speech tags from the Tagger (`token.pos_`) or sentence spans from the Sentencizer (`doc.sents`).\n",
    "\n",
    "If you want to test your implementation, you can continue to execute the following blocks. If you do this repeatedly, do not forget to reload every block again, starting from the block that defines your new pipeline component above.\n",
    "\n",
    "The section **A possible implementation** below also provides an example implementation for a perspective tagger."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72a77b79",
   "metadata": {},
   "source": [
    "To build your pipeline, you have to **1)** import the pipeline components you need (if you need any) analogously to the Sentencizer below, and **2)** add the components to the pipeline. In the last line below, we add the perspective tagger to the pipeline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "36eb70fa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# import pipeline components\n",
    "sys.path.append(os.path.join(os.getcwd(), \"../pipy\"))\n",
    "from pipeline.components.sentencizer import spacy_sentencizer\n",
    "from settings import PARSING_PATH\n",
    "\n",
    "# Create pipeline object\n",
    "nlp = spacy.load(os.path.join(PARSING_PATH, \"de_ud_lg\"))\n",
    "# Add components\n",
    "nlp.add_pipe(spacy_sentencizer, name=\"sentencizer\", before=\"parser\")\n",
    "nlp.add_pipe(rb_perspective_tagger, name=\"perspective_tagger\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61bed5d7",
   "metadata": {},
   "source": [
    "You can print `nlp.pipe_names` to show the components that are part of the pipeline:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d6bd8c1b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['tagger', 'sentencizer', 'parser', 'ner', 'perspective_tagger']\n"
     ]
    }
   ],
   "source": [
    "print(nlp.pipe_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "59a0f814",
   "metadata": {},
   "source": [
    "We use Theodor Fontane's *Der Stechlin* and Gottfried August Bürger's *Münchhausen* to test the perspective tagger. *Der Stechlin* has a 3rd-person narrator, whereas *Münchhausen* has a 1st-person narrator.\n",
    "We load the texts from TextGrid below (feel free to load other texts as well):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "864f1187",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the text from TextGrid repository:\n",
    "text_fontane_stechlin = aggregator.text(\"textgrid:n143.0\").text\n",
    "text_buerger_muenchhausen = aggregator.text(\"textgrid:ktbv.0\").text"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7efcbb33",
   "metadata": {},
   "source": [
    "Pipe the texts through spaCy pipeline:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "ce67c741",
   "metadata": {},
   "outputs": [],
   "source": [
    "doc_fontane_stechlin = nlp(text_fontane_stechlin)\n",
    "doc_buerger_muenchhausen = nlp(text_buerger_muenchhausen)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fec05a7",
   "metadata": {},
   "source": [
    "Get the perspective of each text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "3ebfb741",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3rd\n",
      "3rd\n"
     ]
    }
   ],
   "source": [
    "print(doc_fontane_stechlin._.perspective) # should be `3rd`\n",
    "print(doc_buerger_muenchhausen._.perspective) # should be `1st`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04ddab1a",
   "metadata": {},
   "source": [
    "Sometimes it helps to inspect the text or attributes of sentences, clauses or tokens. The following box is meant to be a \"play area\", where you can print whatever you need. Currently, it prints every sentence (among the first 20) that contains any of the tokens *ich* 'I', *mich* 'me', *er* 'he', or *ihm* 'him', together with the part-of-speech tag and the dependency relation of the token."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "c938c3b6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PRON nsubj \n",
      "Wunderbare Reisen zu Wasser und zu Lande Feldzüge und lustige Abenteuer des Freiherrn von Münchhausen,\n",
      "wie er dieselben bei der Flasche im Zirkel seiner Freunde selbst zu erzählen pflegt. \n",
      "\n",
      "PRON nsubj \n",
      "Ich trat meine Reise nach Rußland von Haus ab mitten im Winter an, weil ich ganz richtig schloß, daß Frost und Schnee die Wege durch die nördlichen Gegenden von Deutschland, Polen, Kur- und Livland, welche nach der Beschreibung aller Reisenden fast noch elender sind als die Wege nach dem Tempel der Tugend, endlich, ohne besondere Kosten hochpreislicher, wohlfürsorgender Landesregierungen, ausbessern müßte. \n",
      "\n",
      "PRON nsubj Ich reisete zu Pferde, welches, wenn es sonst nur gut um Gaul und Reiter steht, die bequemste Art zu reisen ist. \n",
      "\n",
      "PRON nsubj:pass Ich war nur leicht bekleidet, welches ich ziemlich übel empfand, je weiter ich gegen Nordost hin kam. \n",
      "\n",
      "PRON nsubj \n",
      "Nun kann man sich einbilden, wie bei so strengem Wetter, unter dem rauhesten Himmelsstriche, einem armen, alten Manne zumute sein mußte, der in Polen auf einem öden Anger, über den der Nordost hinschnitt, hilflos und schaudernd dalag und kaum hatte, womit er seine Schamblöße bedecken konnte. \n",
      "\n",
      "PRON nsubj Ob mir gleich selbst das Herz im Leibe fror, so warf ich dennoch meinen Reisemantel über ihn her. \n",
      "\n",
      "PRON obj »Hol' mich der Teufel, mein Sohn, das soll dir nicht unvergolten bleiben! \n",
      "\n",
      "PRON nsubj \n",
      "Ich ließ das gut sein und ritt weiter, bis Nacht und Dunkelheit mich überfielen. \n",
      "\n",
      "PRON nsubj und ich wußte weder Weg noch Steg. \n",
      "\n"
     ]
    }
   ],
   "source": [
    "for sent in list(doc_buerger_muenchhausen.sents)[0:20]:\n",
    "    for token in sent:\n",
    "        if token.text.lower() in [\"ich\", \"mich\", \"er\", \"ihm\"]:\n",
    "            print(token.pos_, token.dep_, sent, \"\\n\")\n",
    "            break"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2f518b4",
   "metadata": {},
   "source": [
    "## A possible implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a60a12ff",
   "metadata": {},
   "source": [
    "A rule-based perspective tagger could look like the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "329bcaa6",
   "metadata": {},
   "outputs": [],
   "source": [
    "def rb_perspective_tagger(doc):\n",
    "    \"\"\"Spacy pipeline component.\n",
    "        Adds the perspective of the text's narrator (\"1st\", \"2nd\" or \"3rd\") to the document.\n",
    "\n",
    "    Args:\n",
    "        doc (`Doc`): A spacy document object.\n",
    "    \n",
    "    Returns:\n",
    "        `Doc`: A spacy document object.\n",
    "\n",
    "    \"\"\"\n",
    "    # define two new extensions:\n",
    "    add_extension(Doc, \"perspective\") # receives the most likely perspective (\"1st\", \"2nd\" or \"3rd\") as string\n",
    "    add_extension(Doc, \"perspective_stats\") # receives probablities for each perspective\n",
    "    \n",
    "    # the for loop iterates over every token in the document and counts\n",
    "    # how often pronouns of each person (\"1st\", \"2nd\" or \"3rd\") occur;\n",
    "    # pronouns within direct speech are ignored\n",
    "    pron_person_counts = {}\n",
    "    for token in doc:\n",
    "        if token.pos_ == \"PRON\" and not (\"direct\" in token._.speech):\n",
    "            if token._.morph.person is not None:\n",
    "                if token._.morph.person not in pron_person_counts:\n",
    "                    pron_person_counts[token._.morph.person] = 1\n",
    "                else:\n",
    "                    pron_person_counts[token._.morph.person] += 1\n",
    "    \n",
    "    # the perspective of the document is set to the person of the most pronouns\n",
    "    doc._.perspective = max(pron_person_counts, key=pron_person_counts.get)\n",
    "    \n",
    "    # additionally, the distribution of person in pronouns is saved in the `perspective_stats` attribute;\n",
    "    # for this, the counts are converted to percentages\n",
    "    total_counts = sum(pron_person_counts.values())\n",
    "    doc._.perspective_stats = {person : 1.0*pron_person_counts[person]/total_counts for person in pron_person_counts}\n",
    "    \n",
    "    # return the document\n",
    "    return doc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1db04cd",
   "metadata": {},
   "source": [
    "The perspective tagger from above requires the `.morph` extension from the morphological Analyzer and the `.speech` from the SpeechTagger, which are loaded and added below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "a7b2d318",
   "metadata": {},
   "outputs": [],
   "source": [
    "# import pipeline components\n",
    "sys.path.append(os.path.join(os.getcwd(), \"../pipy\"))\n",
    "from pipeline.components.analyzer import demorphy_analyzer\n",
    "from pipeline.components.speech_tagger import quotation_marks_speech_tagger\n",
    "from pipeline.utils_methods import add_extension\n",
    "from settings import PARSING_PATH\n",
    "\n",
    "# build the pipeline\n",
    "nlp = spacy.load(os.path.join(PARSING_PATH, \"de_ud_lg\"))\n",
    "nlp.add_pipe(demorphy_analyzer, name=\"analyzer\")\n",
    "nlp.add_pipe(quotation_marks_speech_tagger, name=\"speech_tagger\")\n",
    "nlp.add_pipe(rb_perspective_tagger, name=\"perspective_tagger\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c3df41e",
   "metadata": {},
   "source": [
    "Pipe the two test texts:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "dbe2d43b",
   "metadata": {},
   "outputs": [],
   "source": [
    "doc_fontane_stechlin = nlp(text_fontane_stechlin)\n",
    "doc_buerger_muenchhausen = nlp(text_buerger_muenchhausen)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "104848de",
   "metadata": {},
   "source": [
    "And print the values of the new custom attributes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "7744bb27",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3per {'3per': 0.9745269286754003, '1per': 0.023289665211062592, '2per': 0.002183406113537118}\n",
      "1per {'3per': 0.36978851963746223, '1per': 0.6265861027190333, '2per': 0.0036253776435045317}\n"
     ]
    }
   ],
   "source": [
    "print(doc_fontane_stechlin._.perspective, doc_fontane_stechlin._.perspective_stats)\n",
    "print(doc_buerger_muenchhausen._.perspective, doc_buerger_muenchhausen._.perspective_stats)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env_pipy_public",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

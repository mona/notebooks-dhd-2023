"""
Only for workshop organisers 🙀
"""

# import statements
import os
import spacy
import sys

# import pipeline components
#sys.path.append('/scratch/projects/dhd2023/pipy')
sys.path.append('../pipy')
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.coref import rb_coref
from pipeline.components.entity_linker import wikidata_entity_linker
from pipeline.components.event_tagger import event_event_tagger
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.reflection_tagger import neural_reflection_tagger
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.speech_tagger import flair_speech_tagger, quotation_marks_speech_tagger

from settings import PARSING_PATH, PICKLE_PATH

def pickle_wrap(doc, func):
    # define the pickle wrapper
    return pickle_wrapper(doc, func, load_output=True, save_output=False, overwrite=False, pickle_path=PICKLE_PATH)

# create a pipeline that loads pre-computed results
def load_pipeline_with_precomputed_neural_results():
    # create pipeline object
    nlp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
    # add components that use pre-computed results
    nlp.add_pipe(lambda doc: pickle_init(doc, "de_ud_lg", "max_sent_slicer", -1), name="pickle_init", before="tagger")
    nlp.add_pipe(lambda doc: pickle_wrap(doc, spacy_sentencizer), name="sentencizer", before="parser")
    nlp.add_pipe(lambda doc: pickle_wrap(doc, dependency_clausizer), name="clausizer")
    nlp.add_pipe(lambda doc: pickle_wrap(doc, flair_speech_tagger), name="speech_tagger")
    nlp.add_pipe(lambda doc: pickle_wrap(doc, event_event_tagger), name="event_tagger")
    nlp.add_pipe(lambda doc: pickle_wrap(doc, neural_reflection_tagger), name="reflection_tagger")
    return nlp

if __name__ == '__main__':
    from tgclients.aggregator import Aggregator
    aggregator = Aggregator()

    text_buerger_muenchhausen = aggregator.text("textgrid:ktbv.0").text
    text_fontane_stechlin = aggregator.text("textgrid:n143.0").text
    text_goethe_wahlverwandtschaften = aggregator.text("textgrid:11hnp.0").text
    text_kafka_prozess = aggregator.text("textgrid:qmx4.0").text
    text_kafka_verwandlung = aggregator.text("textgrid:qn07.0").text
    text_verne_reise = aggregator.text("textgrid:wr7p.0").text

    texts = list()
    texts.append(text_buerger_muenchhausen)
    texts.append(text_fontane_stechlin)
    texts.append(text_goethe_wahlverwandtschaften)
    texts.append(text_kafka_prozess)
    texts.append(text_kafka_verwandlung)
    texts.append(text_verne_reise)

    nlp = load_pipeline_with_precomputed_neural_results()
    for text in texts:
        nlp(text)
